import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Dinda Mutiara Qur'ani Putri, NPM 1706984556 , Kelas DDP 2 - D, GitLab Account: dmqputri
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga     : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah             : ");
		String alamat = input.nextLine();
		
		System.out.print("Panjang Tubuh (cm)       : ");
		String panjang = input.nextLine();
		int panjangInt = Integer.parseInt(panjang);
			if (panjangInt < 0 || panjangInt > 250){
				System.out.println("Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Lebar Tubuh (cm)         : ");
		String lebar = input.nextLine();
		int lebarInt = Integer.parseInt(lebar);
			if (lebarInt < 0 || lebarInt > 250){
				System.out.print("Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Tinggi Tubuh (cm)        : ");
		String tinggi = input.nextLine();
		int tinggiInt = Integer.parseInt(tinggi);
			if (tinggiInt < 0 || tinggiInt > 250){
				System.out.print("WARNING : Keluarga ini tidak perlu direlokasi!");
				System.exit(0); // bakal keluar
			}
		System.out.print("Berat Tubuh (kg)         : ");
		String berat = input.nextLine();
		float beratInt = Float.parseFloat(berat);
			if (beratInt < 0 || beratInt > 150){
				System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Jumlah Anggota Keluarga  : ");
		String anggota = input.nextLine();
		int anggotaInt = Integer.parseInt(anggota);
			if (anggotaInt < 0 || anggotaInt > 20){
				System.out.println("WARNING : Keluarga ini tidak perlu direlokasi!");
				System.exit(0);
			}
		System.out.print("Tanggal Lahir(dd-mm-yyyy): ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan         : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data      : ");
		String jumlahCetakan = input.nextLine();
		int jumlahCetakanInt = Integer.parseInt(jumlahCetakan);


		// Soal Tutorial "Sensus Daerah Kumuh"
		// menghitung rasio berat per volume 
		int rasio =(int)(beratInt*1000000)/((panjangInt)*(lebarInt)*(tinggiInt));
		
		for (int cetak = 1; cetak < jumlahCetakanInt + 1; cetak++ ) {
			// minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + cetak + " dari " + jumlahCetakanInt + " untuk: ");
			String penerima = input.nextLine(); 
			String penerimaUpper = penerima.toUpperCase();
			

			// Periksa ada catatan atau tidak
			if (catatan.length() == 0 ){
				catatan = ("Tidak ada catatan tambahan");
			}
			

			// Cetak hasil 
			String hasil = "DATA SIAP DICETAK UNTUK " + penerimaUpper + "\n" +
					"---------------------\n" +
					nama + " - " + alamat + "\n" +
					"Lahir pada tanggal " + tanggalLahir + "\n" +
					"Rasio Berat Per Volume = " + String.valueOf(rasio)+ " kg/m^3" + "\n" +
					"Catatan: " + catatan;
			System.out.println(hasil);
		}


		// soal bonus "Rekomendasi Apartemen"
		// Hitung nomor keluarga dari parameter yang telah disediakan 

		char hurufPertama = nama.charAt(0);
		int ascii = 0;
		for (int i = 0; i < nama.length(); i++){
			ascii += nama.charAt(i);
		}
		int kalkulasi = ((panjangInt*tinggiInt*lebarInt)+ ascii)%10000;
		String kalkulasiFinal = String.valueOf(kalkulasi);


		// menggabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = hurufPertama + kalkulasiFinal;

		// menghitung anggaran makanan per tahun 
		int anggaran =(50000)*(365)*anggotaInt;

		// menghitung umur dari tanggalLahir 
		String tahunLahir = tanggalLahir.substring(6,10);		
		int tahunLahirInt = Integer.parseInt(tahunLahir);
		 
		int umur = 2018-(tahunLahirInt);

		// proses menentukan apartemen
		String kabupaten = "";
		String namaApart = "";
		if (umur >= 0 && umur <= 18){
			namaApart = "PPMT";
			kabupaten = "Rotunda";
		}
		else if ( (umur >= 19 && umur <= 1018) && (anggaran >= 0 && anggaran <= 100000000)) {
			namaApart = "Teksas";
			kabupaten = "sastra";
		}
		else if((umur >= 19 && umur <= 1018)&&(anggaran >= 100000000)){
			namaApart = "Mares";
			kabupaten = "Margonda";
		}

		// mencetak rekomendasi 
		String rekomendasi = ("\n" + "REKOMENDASI APARTEMEN" + 
				"\n" + "--------------------" +
				"\n" + "MENGETAHUI: Identitas keluarga: " + nama + " - " + nomorKeluarga +
				"\n" + "MENIMBANG:  Anggaran makanan tahunan: Rp " +anggaran+
				"\n" + "            Umur kepala keluarga: " +umur+ " tahun" +
				"\n" + "MEMUTUSKAN: Keluarga " + nama + " akan ditempatkan di: " +
				"\n" + namaApart + ", kabupaten " + kabupaten + 
				"\n");
    
		System.out.println(rekomendasi);

		input.close();
	}
}
